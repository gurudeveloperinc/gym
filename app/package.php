<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class package extends Model
{
    protected $primaryKey = 'pid';

	public function Activities(){
		return $this->hasMany('App\package_content','pid','pid');
	}
}
