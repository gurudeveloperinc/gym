<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class packagePurchases extends Model
{
	protected $primaryKey = 'purid';
	public $table = 'package_purchases';

	public function User() {
		return $this->belongsTo('App\User','uid','uid');
	}

	public function Package() {
		return $this->belongsTo('App\package','pid','pid');
	}
}
