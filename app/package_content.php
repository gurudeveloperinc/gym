<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class package_content extends Model
{
    protected $primaryKey = 'pcid';
	protected $table = 'package_content';

	public function Activity(){
		return $this->hasOne('App\activity','aid','aid');
	}
}
