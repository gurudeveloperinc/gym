<?php

namespace App\Http\Controllers;

use App\packagePurchases;
use App\package;
use App\activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function index(){
	    $packages = package::all()->sortByDesc('created_at')->take(3);
	    $activities = activity::all()->sortByDesc('created_at')->take(3);
	    return view('welcome',[
		    'packages' => $packages,
		    'activities' => $activities
	    ]);
    }

	public function postPurchasePackage( Request $request ) {

		$purchase = new packagePurchases();
		$purchase->uid = Auth::user()->uid;
		$purchase->pid = $request->input('pid');
		$purchase->transaction = $request->input('transaction');
		$purchase->save();

		return response("success");
	}
}
