<?php

namespace App\Http\Controllers;

use App\activity;
use App\activityPurchases;
use App\assignment;
use App\package;
use App\package_content;
use App\packagePurchases;
use App\User;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
	    $paid = "0";
	    $assigned = "[]";

	    if(Auth::user()->role == "Customer"){

	        $purchases = packagePurchases::all();

		    foreach($purchases as $item){
			    if(Auth::user()->uid == $item->uid){
				    $paid = "1";
			    }
		    }

		       $assigned = assignment::where('client',Auth::user()->uid)->first();
			    if(!empty($assigned))
				    $assigned = User::find($assigned->instructor);

	    }



	    if(Auth::user()->role == "Instructor"){
		    $assigned = assignment::where('instructor',Auth::user()->uid)->get();

		    $assignedArray = array();
		    foreach($assigned as $item){
			    array_push($assignedArray, User::find($item->client));
		    }
		    $assigned = $assignedArray;
	    }



	    $transaction = packagePurchases::all();
	    $packages = package::all();
	    $activities = activity::all();
	    $customers = User::where('role','Customer')->get();
	    $instructors = User::where('role','Instructor')->get();
	    $assignments = assignment::all()->count();
        $pending = $customers->count() - $assignments;
	    return view('home',[
	        'customers' => $customers,
	        'instructors' => $instructors,
	        'packages' => $packages,
	        'activities' => $activities,
	        'pending' => $pending,
		    'paid' => $paid,
		    'assigned' => $assigned,
		    'transactions' => $transaction
        ]);
    }

	public function customerReport() {
		$customers = User::where('role','customer')->get();
		
		return view('customerReport',[
			'customers' => $customers
		]);
	}


	public function paymentReport() {

		$transaction = packagePurchases::all()->sortByDesc('created_at');

		return view('paymentReport',[
			'transactions' => $transaction
		]);
	}
	public function downloadCustomerPDF() {
		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->customerReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'landscape' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}


	}

	public function downloadPaymentPDF() {
		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->paymentReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}


	}



	public function getManagePackages() {

		$packages = package::all();

		return view('managePackages',[
			'packages' => $packages
		]);
	}


	public function getManageActivities() {
		$activities = activity::all();
		return view('manageActivities',[
			'activities' => $activities
		]);
	}
	public function getViewPackage($pid) {
		$package = package::find($pid);
		$activity = activity::all();

		return view('viewPackage',[
			'package' => $package,
			'activities' => $activity
		]);
	}

	public function getCreatePackage() {
		return view('createPackage');
	}

	public function getChangeProfilePic() {
		return view('changeProfilePic');
	}

	public function deletePackage( Request $request,$pid ) {
		package::destroy($pid);
		$request->session()->flash('success','Package Deleted');
		return redirect('/manage-packages');
	}

	public function viewProfile( $id ) {

		$user = User::find($id);

		if($user->role == "Customer"){
			$assigned = assignment::where('client',$user->uid)->first();
			if(!empty($assigned))
			$assigned = User::find($assigned->instructor);
		}

		if($user->role == "Instructor"){
			$assigned = assignment::where('instructor',$user->uid)->get();

			$assignedArray = array();
			foreach($assigned as $item){
				array_push($assignedArray, User::find($item->client));
			}
			$assigned = $assignedArray;
		}


		$instructors = User::where('role',"Instructor")->get();

		return view('viewProfile',[
			'user' => $user,
			'instructors' => $instructors,
			'assigned' => $assigned
		]);
	}

	public function postAssignInstructor( Request $request ) {
		$assignment = new assignment();
		$assignment->instructor = $request->input('instructor');
		$assignment->client = $request->input('client');
		$assignment->save();

		$request->session()->flash("success", "Assigned successfully");
		return redirect('/home');
	}

	public function postCreateActivity( Request $request ) {
		$activity = new activity();
		$activity->title = $request->input('title');
		$activity->description = $request->input('description');
		$activity->price = $request->input('price');
		$activity->uid = Auth::user()->uid;
		$activity->save();

		$request->session()->flash("success", "Activity Created Successfully");

		return redirect('/manage-activities');
	}

	public function postAddPackageActivity( Request $request) {

		$aid = $request->input('aid');
		$packageContent  = new package_content();
		$packageContent->pid = $request->input('pid');
		$packageContent->aid = $aid;
		$packageContent->save();

		$request->session()->flash("success", "Activity Added Successfully");

		return redirect('/view-package/' . $aid);
	}

	public function postRemovePackageActivity( Request $request) {

		$aid = $request->input('aid');
		$pid = $request->input('pid');
		$packageContent  = package_content::where('aid',$aid)->where('pid',$pid)->delete();

		$request->session()->flash("success", "Activity Removed Successfully");

		return redirect('/view-package/' . $aid);
	}

	public function postChangeProfilePic(Request $request) {

		if($request->hasFile('photo')){
			$fileName = $request->file('photo')->getClientOriginalName();
			$request->file('photo')->move('uploads/profilePics', $fileName);

			$user = User::find(Auth::user()->uid);
			$user->photo = url('uploads/profilePics/' .$fileName);
			$status = $user->save();

			if($status)
				$request->session()->flash('success','Profile photo changed');
			else
				$request->session()->flash('error','Sorry an error occurred. Please try again.');

			return redirect('/change-profile-pic');
		}
		$request->session()->flash('error','No photo uploaded. Please try again.');
		return redirect('/change-profile-pic');
	}
}
