<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class activityPurchases extends Model
{
	protected $primaryKey = 'purid';
	protected $table = 'activity_purchases';
}
