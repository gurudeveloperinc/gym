<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');

Route::get('/manage-packages','HomeController@getManagePackages');
Route::get('/create-package','HomeController@getCreatePackage');

Route::get('/view-package/{pid}','HomeController@getViewPackage');
Route::get('/delete-package/{pid}','HomeController@deletePackage');
Route::get('/change-profile-pic','HomeController@getChangeProfilePic');


Route::get('/manage-activities','HomeController@getManageActivities');

Route::get('/view-profile/{id}','HomeController@viewProfile');
Route::get('/customer-report','HomeController@downloadCustomerPDF');
Route::get('/payment-report','HomeController@downloadPaymentPDF');

Route::post('/create-activity','HomeController@postCreateActivity');
Route::post('/add-package-activity','HomeController@postAddPackageActivity');
Route::post('/purchase-package','PublicController@postPurchasePackage');
Route::post('/remove-package-activity','HomeController@postRemovePackageActivity');
Route::post('/change-profile-pic','HomeController@postChangeProfilePic');
Route::post('/assign-instructor','HomeController@postAssignInstructor');



Auth::routes();

Route::get('/home', 'HomeController@index');
