@extends('layouts.app')

@section('content')

    @include('topMenu')


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        @foreach($errors->all() as $item)
                        {{$item}}

                        @endforeach
                        <div class="form-group {{ $errors->has('fname') ? ' has-error' : '' }}">
                            <label for="fname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6 ">
                                <input pattern="[A-Za-z]+" id="fname" type="text" class="form-control" name="fname" value="{{ old('fname') }}" required autofocus>

                                @if ($errors->has('fname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>


                        </div>


                         <div class="form-group">
                             <label for="mname" class="col-md-4 control-label">Middle Name</label>

                             <div class="col-md-6 ">
                                 <input id="mname" type="text" class="form-control" name="mname" value="{{ old('mname') }}" >

                             </div>
                         </div>




                         <div class="form-group">
                             <label for="sname" class="col-md-4 control-label">Surname</label>

                             <div class="col-md-6">
                                 <input id="sname" type="text" class="form-control" name="sname" value="{{ old('sname') }}" >

                             </div>
                         </div>

                        
                                 
                                 
                                 <div class="form-group">
                                     <label for="age" class="col-md-4 control-label">Date Of Birth</label>
                                 
                                     <div class="col-md-6">
                                         <input required id="age" type="text" class="form-control" pattern="[0-9]{1,2}/[0-9]+/[0-9]+" placeholder="12/05/1990" name="age" value="{{ old('age') }}" >
                                     </div>
                                 </div>


                                 <div class="form-group">
                                     <label for="postalAddress" class="col-md-4 control-label">Postal Address</label>

                                     <div class="col-md-6">
                                            <textarea required style="width: 100%" name="postalAddress">{{old('postalAddress')}}</textarea>
                                     </div>
                                 </div>

                                 
                                 <div class="form-group">
                                     <label for="residentialAddress" class="col-md-4 control-label">Residential Address</label>
                                     
                                     <div class="col-md-6">
                                         <textarea style="width: 100%" name="residentialAddress">{{old('residentialAddress')}}</textarea>
                                      </div>
                                 </div>
                        
                                 
                                 <div class="form-group">
                                     <label for="officePhone" class="col-md-4 control-label">Office Phone</label>
                                     
                                     <div class="col-md-6">
                                         <input id="officePhone" type="text" class="form-control" name="officePhone" value="{{ old('officePhone') }}" >
                                     </div>
                                 </div>
                                 
                                 
                                 <div class="form-group">
                                     <label for="homePhone" class="col-md-4 control-label">Home Phone</label>
                                     
                                     <div class="col-md-6">
                                         <input id="homePhone" type="text" class="form-control" name="homePhone" value="{{ old('homePhone') }}" >
                                     </div>
                                 </div>
                                 
                                 
                                 <div class="form-group">
                                     <label for="reason" class="col-md-4 control-label">Reason for joining</label>
                                     
                                     <div class="col-md-6">
                                         <textarea style="width: 100%" name="reason">{{old('reason')}}</textarea>
                                     </div>
                                 </div>

                            <div class="form-group">
                                <label for="complications" class="col-md-4 control-label">Health Complications</label>

                                <div class="col-md-6">
                                    <textarea style="width: 100%" name="complications">{{old('complications')}}</textarea>
                                </div>
                            </div>


                                 <div class="form-group">
                                     <label for="weight" class="col-md-4 control-label">Weight</label>

                                     <div class="col-md-6">
                                         <input id="weight" type="text" class="form-control" name="weight" value="{{ old('weight') }}" >
                                     </div>
                                 </div>


                                 <div class="form-group">
                                     <label for="height" class="col-md-4 control-label">Height</label>

                                     <div class="col-md-6">
                                         <input id="height" type="text" class="form-control" name="height" value="{{ old('height') }}" >
                                     </div>
                                 </div>





                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label" style="margin-right: 15px">Role</label>

                            <select required name="role" class="form-control" style="width: 40%">
                                <option>Instructor</option>
                                <option>Customer</option>
                            </select>
                        </div>



                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
