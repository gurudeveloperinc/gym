<html>
<head>

    <link href="{{url('css/bootstrap.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="col-md-12 main " >
    <div>
        <img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
        <h3 class="logoHeader">Customer report </h3>
    </div>

    <style>
        td{
            text-align: center;
        }

    </style>

    <h3 style="color:#B79043">CUSTOMER REPORT</h3>
    <table  class="table table-hover ">
        <tr>
            <th>First</th>
            <th>Middle Name</th>
            <th>Surname</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Postal Address</th>
            <th>Residential Address</th>
            <th>Home Phone</th>
            <th>Office Phone</th>

        </tr>

        @foreach($customers as $item)
            <tr>
                <td>{{$item->fname}}</td>
                <td>{{$item->mname}}</td>
                <td>{{$item->sname}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->postalAddress}}</td>
                <td>{{$item->residentialAddress}}</td>
                <td>{{$item->homePhone}}</td>
                <td>{{$item->officePhone}}</td>
            </tr>
        @endforeach


    </table>

</div>
</body>
</html>