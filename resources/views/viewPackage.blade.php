@extends('layouts.app')

@section('content')

     @include('topMenu')

    <div class="container viewPackage ">

        @if( Session::has('success') )
            <div class="alert alert-success" align="center">{{ Session::get('success')}}</div>
        @endif

        @if( Session::has('error') )
            <div class="alert alert-danger" align="center">{{ Session::get('error')}}</div>
        @endif

        <div class="panel panel-default col-md-8 col-md-offset-2  ">
            <div class="panel-heading">
                {{$package->title}}
            </div>

            <div class="panel-body">

                <div class="row">
                    @foreach($package->Activities as $content)
                    <div class="col-md-4">
                        <h3>{{$content->Activity->title}}</h3>
                        <p>{{$content->Activity->description}}</p>
                    </div>

                    @endforeach

                </div>

            </div>

        </div>

        <div class="row col-md-6" align="center">

            <h3>Add Activity</h3>

            <form method="post" action="{{url('/add-package-activity')}}">

                {{csrf_field()}}

            <input type="hidden" name="pid" value="{{$package->pid}}">

            <select name="aid" class="form-control" style="width: 200px;" name="activity">
                @foreach($activities as $item)
                    <option value="{{$item->aid}}">{{$item->title}}</option>
                @endforeach

            </select> <br>

            <button type="submit" class="btn btn-success">Add</button>
            </form>
        </div>
            <div class="row col-md-6" align="center">

                <h3>Remove Activity</h3>

                <form method="post" action="{{url('/remove-package-activity')}}">

                    {{csrf_field()}}

                    <input type="hidden" name="pid" value="{{$package->pid}}">

                    <select name="aid" class="form-control" style="width: 200px;" name="activity">
                        @foreach($activities as $item)
                            <option value="{{$item->aid}}">{{$item->title}}</option>
                        @endforeach

                    </select> <br>

                    <button type="submit" class="btn btn-danger">Remove</button>
                </form>
            </div>

    </div>

@endsection