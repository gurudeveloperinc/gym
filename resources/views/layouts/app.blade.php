<!DOCTYPE html>
<html>
<head>
    <title>Maximum Gym Dansoman | Home </title>
    <link href="{{url('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{url('css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Gym Lite Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <!--fonts-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
    <!--//fonts-->
    <script src="{{url('js/jquery.min.js')}}"> </script>
    <!--start slider -->
    <link rel="stylesheet" href="{{url('css/flexslider.css')}}" type="text/css" media="screen" />
    <!--end slider -->
    <link rel="stylesheet" href="css/swipebox.css">
    <script src="js/jquery.swipebox.min.js"></script>
    <script type="text/javascript">
        jQuery(function($) {
            $(".swipebox").swipebox();
        });
    </script>
</head>
<body>

@yield('content')

        <!--footer-->
<div class="fotter">
    <div class="container">
        <p class="fotter-info">Copyrights &copy; 2017 Maximum Gym . All rights reserved | Designed by <a href="http://regent.edu.gh/">Sellasie</a></p>
        <div class="social-icons">
            <a href="#"><span class="beh"> </span></a>
            <a href="#"><span class="face"> </span></a>
            <a href="#"><span class="pin"> </span></a>
            <a href="#"><span class="br"> </span></a>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--/footer-->
</body>
</html>

