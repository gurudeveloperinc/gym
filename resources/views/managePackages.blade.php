@extends('layouts.app')

@section('content')

     @include('topMenu')

    <div class="container">
        <table class="table table-responsive">
            <tr>
                <th>Title</th>
                <th></th>
            </tr>

            @foreach($packages as $package)
            <tr>
                <td>{{$package->title}}</td>
                <td><a href="{{url('/view-package/' . $package->pid)}}" class="btn btn-primary">View</a></td>
                <td><a href="{{url('/delete-package/' . $package->pid)}}" class="btn btn-danger">Delete</a></td>
            </tr>
            @endforeach

        </table>
    </div>

@endsection