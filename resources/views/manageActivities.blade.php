@extends('layouts.app')

@section('content')

     @include('topMenu')

    <div class="container viewActivity">
        @if( Session::has('success') )
                 <div class="alert alert-success" align="center">{{ Session::get('success')}}</div>
        @endif

        @if( Session::has('error') )
            <div class="alert alert-danger" align="center">{{ Session::get('error')}}</div>
        @endif


        <div class="panel panel-default">
            <div class="panel-heading">
                Activities
            </div>
            <div class="panel-body">
                <table class="table table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Price</th>
                        <th></th>
                    </tr>

                    @foreach($activities as $item)
                        <tr>
                            <td>{{$item->title}}</td>
                            <td>{{$item->price}}</td>
                            <td>
                                <a href="{{url('/view-activity/' . $item->aid)}}" class="btn btn-primary">View</a>
                            </td>
                        </tr>
                    @endforeach
                </table>

                <div class="row col-md-12" align="center">
                   <h3>Add Activity</h3>
                    <form method="post" action="{{url('/create-activity')}}">

                        {{csrf_field()}}
                         <div class="form-group col-md-6 col-lg-offset-3">


                             <label for="title" class="control-label">Title</label>
                             <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" >


                            <label class="control-label">Descripton</label>
                            <textarea class="form-control" name="description">{{old('description')}}</textarea>


                             <label for="price" class="control-label">Price</label>
                             <input id="price" type="text" class="form-control" name="price" value="{{ old('price') }}" >


                             <button type="submit" class="btn btn-primary">Add</button>
                             <button type="reset" class="btn btn-danger">Clear</button>

                         </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection