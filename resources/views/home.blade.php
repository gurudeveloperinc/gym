@extends('layouts.app')

@section('content')

    @include('topMenu')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            @if( Session::has('success') )
                <div class="alert alert-success" style="margin-top: 10px;"  align="center">{{Session::get('success')}}</div>
            @endif

            @if( Session::has('error') )
                <div class="alert alert-success" style="margin-top: 10px" align="center">{{Session::get('error')}}</div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard for {{Auth::user()->fname}} {{Auth::user()->sname}}
                    <br>You are a {{Auth::user()->role}}
                </div>

                <div class="panel-body">

                    {{-- manager --}}
                    <div>
                        <div align="center" class="row">
                            <div align="center">
                                <img src="{{Auth::user()->photo}}" style="width:150px;" > <br>
                                <a class="btn btn-primary" href="{{url('/change-profile-pic')}}">Change Profile Pic</a>
                            </div>
                        </div>

                        @if(Auth::user()->role == "Instructor")
                            <div class="row">
                            <h3>Customers you are assigned to</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                </tr>
                                @foreach($assigned as $item)
                                    <tr>
                                        <td>
                                            <a href="{{url('/view-profile/'. $item->uid)}}">
                                                {{$item->fname}} {{$item->sname}}
                                            </a>
                                        </td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->phone}}</td>
                                    </tr>
                                @endforeach
                            </table>

                            </div>
                        @endif


                    @if(Auth::user()->role == 'Manager')


                        <div class="row">
                            <div align="center" class="col-md-6">
                                There are {{count($packages)}} packages in the system. <br><br>
                                <a class="btn btn-primary" href="{{url('/manage-packages')}}">Manage Packages</a>
                            </div>


                            <div align="center" class="col-md-6">
                                There are {{count($activities)}} activities in the system. <br><br>
                                <a class="btn btn-primary" href="{{url('/manage-activities')}}">Manage Activities</a>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="well well-lg" align="center">{{count($instructors)}} Instructors</div>
                            </div>

                            <div class="col-md-4">
                                <div class="well well-lg" align="center">{{count($customers)}} Customers</div>
                            </div>

                            <div class="col-md-4">
                                <div class="well well-lg" align="center">{{$pending}} Pending Assignment</div>
                            </div>

                        </div>
                        <h3>Instructors</h3>
                        <table class="table table-responsive">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                            @foreach($instructors as $item)
                             <tr>
                                 <td>
                                     <a href="{{url('/view-profile/'. $item->uid)}}">
                                     {{$item->fname}} {{$item->sname}}
                                     </a>
                                 </td>
                                 <td>{{$item->email}}</td>
                                 <td>{{$item->phone}}</td>
                             </tr>
                            @endforeach
                        </table>

                        <hr>

                    </div>




                        <h3 style="display: inline-block;margin-bottom: 20px; margin-right: 20px;">Customers</h3><a style="display: inline-block;" href="{{url('customer-report')}}" class="btn inline btn-success">Download Report</a>
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                            @foreach($customers as $item)
                                <tr>
                                    <td>
                                        <a href="{{url('/view-profile/'. $item->uid)}}">
                                        {{$item->fname}} {{$item->sname}}
                                        </a>
                                    </td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->phone}}</td>
                                </tr>
                            @endforeach
                        </table>

                    <h3 style="display: inline-block;margin-bottom:20px; margin-right:20px;">Payment Transactions</h3> <a style="display: inline-block;" href="{{url('payment-report')}}" class="btn inline btn-success">Download Report</a>
                    <table class="table table-hover">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Txn ID</th>
                        </tr>
                        @foreach($transactions as $item)
                            <tr>
                                <td>
                                    <a href="{{url('/view-profile/'. $item->User->uid)}}">
                                        {{$item->User->fname}} {{$item->User->sname}}
                                    </a>
                                </td>
                                <td>{{$item->User->email}}</td>
                                <td>{{$item->User->phone}}</td>
                                <td>{{$item->transaction}}</td>
                            </tr>
                        @endforeach
                    </table>



                    {{-- end manager --}}
                @endif


                    @if(Auth::user()->role == "Customer" && $paid == "0")

                        You have not purchased any activity or package. <br><br>
                        Please select a package to purchase:

                        <div id="buyContainer">
                            <select>

                                @foreach($packages as $package)
                                <option data-pid="{{$package->pid}}" data-price="{{$package->price}}" >{{$package->title}} : {{$package->price}}</option>
                                    @endforeach
                            </select>

                            <select id="duration">
                                <option value="1">1 month</option>
                                <option value="3">3 months</option>
                                <option value="12">1 year</option>
                            </select>

                            <button class="btn btn-primary" id="buy">Buy</button>

                        </div>

                        <p id="payment" class="hidden">

                        </p>

                        @else

                        @if(Auth::user()->role == "Customer" && !empty($assigned) )

                            <p style="font-size: 24px;" align="center">
                                Assigned to {{$assigned->fname}} {{$assigned->sname}}

                            </p>

                        <style>
                            p.details{
                                font-size: 150% !important;
                            }
                        </style>

                        <div class="col-md-6">
                            <p class="details" >

                                <b>Your Details</b> <br>

                                <b>Name:</b> <span> {{Auth::user()->fname}} {{Auth::user()->mname}} {{Auth::user()->sname}} </span> <br>
                                <b>Email:</b> <span> {{Auth::user()->email}} </span> <br>
                                <b>Phone:</b> <span> {{Auth::user()->phone}}</span> <br>
                                <b>DOB:</b> <span> {{Auth::user()->age}} </span> <br>
                                <b>Postal address:</b><br> <span>{{Auth::user()->postalAddress}}</span> <br>
                                <b>Residential address:</b><br> {{Auth::user()->residentialAddress}} <br>
                                <b>Office Phone:</b> {{Auth::user()->officePhone}} <br>
                                <b>Home Phone:</b> {{Auth::user()->homePhone}} <br>
                                <b>Complications:</b> {{Auth::user()->complications}} <br>
                                <b>Weight:</b> {{Auth::user()->weight}} <br>
                                <b>Height:</b> {{Auth::user()->height}}<br>
                                <b>Reason:</b> {{Auth::user()->reason}} <br>
                                <b>Package:</b> {{Auth::user()->Purchases[count(Auth::user()->Purchases) - 1 ]->Package->title}}


                            </p>

                            </div>
                        <div class="col-md-6">
                            <p class="details">

                                <b>Your Instructor details</b> <br>
                                <b>Name:</b> <span> {{$assigned->fname}} {{$assigned->mname}} {{$assigned->sname}} </span> <br>
                                <b>Email:</b> <span> {{$assigned->email}} </span> <br>
                                <b>Phone:</b> <span> {{$assigned->phone}}</span> <br>
                                <b>DOB:</b> <span> {{$assigned->age}} </span> <br>
                                <b>Postal address:</b><br> <span>{{$assigned->postalAddress}}</span> <br>
                                <b>Residential address:</b><br> {{$assigned->residentialAddress}} <br>
                                <b>Office Phone:</b> {{$assigned->officePhone}} <br>
                                <b>Home Phone:</b> {{$assigned->homePhone}} <br>
                                <b>Complications:</b> {{$assigned->complications}} <br>
                                <b>Weight:</b> {{$assigned->weight}} <br>
                                <b>Fat:</b> {{$assigned->fat}} <br>
                                <b>BMI:</b> {{$assigned->bmi}} <br>
                                <b>Height:</b> {{$assigned->height}}<br>
                                <b>Reason:</b> {{$assigned->reason}}


                            </p>

                        </div>

                            @else

                            @if(Auth::user()->role == "Customer")
                            <p align="center" class="details">Awaiting instructor assignment</p>
                            @endif
                        @endif

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


    <script>
        $(document).ready(function(){
            var pid;
            $('#buy').on('click', function () {
                var price = $('option:selected').data('price');
                pid = $('option:selected').data('pid');

                $('#payment').text('Please pay ' + price + ' into this mobile money account ');
                $('#payment').removeClass('hidden');
                $('#payment').append("<span style='font-size:20px;color:green;'> 0242328822</span>.<br>And put in your transaction id below<br><input type='text' id='transaction'> <br> <br> <button id='send' class='btn btn-success'>Send</button> ");
                $('#buyContainer').addClass('hidden');

                $('#send').click(purchasePackage);
            });

            function purchasePackage() {

                var transaction = $('#transaction').val();

                console.log('in the function');
                $.ajax({
                    url: "{{url('/purchase-package')}}",
                    method: "post",
                    data: { _token: '{{csrf_token()}}', transaction: transaction , pid: pid},
                    success: function (response) {
                        console.log(transaction);
                        console.log(pid);
                        $('#buyContainer').addClass("hide");
                        window.location = "{{url('/home')}}";

                    },
                    error: function (response) {
                        console.log(response.status);
                        console.log(transaction);
                    }
                });
            }

        });
    </script>
@endsection
