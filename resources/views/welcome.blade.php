@extends('layouts.app')

@section('content')
        <!--header-->
<div class="banner">
    <div class="container">
        <div class="banner-right-text">
            <h3>
                @if (Auth::check())
                    <a class="btn btn-primary" href="{{ url('/home') }}">Dashboard</a>
                @else
                    <a class="btn btn-primary" href="{{ url('/login') }}">Login</a>
                    <a class="btn btn-primary" href="{{ url('/register') }}">Register</a>
                @endif
                <br /><span>(+233)</span><i>233423401</i></h3>
        </div>
        <div class="top-menu">
            <span class="menu"> </span>
            <ul class="nav navbar-nav">
                <li><a class="active" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a></li>
                <li><a href='#about'>About</a></li>
                <li><a href="#">Trainers</a></li>
                <li><a href="#testimonial">Testimonials</a></li>
                <li><a href="#gallery">Gallery</a></li>
                <li><a href="#contact">Contact Us</a></li>
                <div class="clearfix"> </div>
            </ul>
        </div>
        <!-- script-for-menu -->
        <script>
            $("span.menu").click(function(){
                $(".top-menu ul").slideToggle("slow" , function(){});
            });
        </script>
        <!-- script-for-menu -->
    </div>
    <div class="middl-text">
        <a href="#"><h1>Maximum GYM</h1></a>
        <p>Its all about fitness</p>
    </div>
</div>
<!--/header-->


        <!--about-->
<div id="about" class="about">
    <div class="container relative">
        <h4>About</h4>
        <div class="col-md-8 about-text">
            <p class="second-text">Maximum gym is a gym located in Dansoman and is registered in ghana we provide the best gym facilities
                in accra.</p>
            <p class="second-text">We’re the Maximum gym, a totally new way to exercise. Affordable, high-spec nationwide gyms, that make absolutely everybody feel welcome.

                Whatever your starting point, and wherever you want to go, we can help.

                So we’re for everyone who’s had enough of the unbelievably-expensive-monthly-contract gym, or the make-me-feel-judged-the-moment-I-walk-through-the-door gym.

                As you might expect we’re growing fast, and there are new Gyms popping up all over the country</p>
        </div>
        <div class="col-md-4 about-slider">
            <section class="slider">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="images/b1.jpg" class="img-responsive" alt="" />
                        </li>
                        <li>
                            <img src="images/b2.jpg" class="img-responsive" alt="" />
                        </li>
                        <li>
                            <img src="images/b1.jpg" class="img-responsive" alt="" />
                        </li>
                        <li>
                            <img src="images/b3.jpg" class="img-responsive" alt="" />
                        </li>
                    </ul>
                </div>
            </section>
            <!-- FlexSlider -->
            <script defer src="js/jquery.flexslider.js"></script>
            <script type="text/javascript">
                $(function(){
                    SyntaxHighlighter.all();
                });
                $(window).load(function(){
                    $('.flexslider').flexslider({
                        animation: "slide",
                        start: function(slider){
                            $('body').removeClass('loading');
                        }
                    });
                });
            </script>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--/about-->
<!--about-->

<!-- packages -->
<div class="find-dog packages">
    <div class="container relative">
        <h4>PACKAGES</h4>

        @foreach($packages as $item)
        <div class="col-md-4 packageItem team-grid-left">
            <div class="team-top">
                <img class="img-responsive" src="images/c1.jpg" alt="">
            </div>
            <h3>{{$item->title}}</h3>

            <ul>

                @foreach($item->Activities as $content)
                  <li>{{$content->Activity->title}}</li>
                @endforeach

            </ul>

           <p class="price"> {{$item->price}} GHC</p>
            <br>
            <a href="{{url('/home')}}" class="btn btn-primary">Buy</a>
        </div>
        @endforeach

    </div>
</div>
<!-- end packages-->

<!--Support-->
<div id="testimonial" class="support">
    <div class="container relative">
        <div class="textimonials-section text-center">
            <h4>Textimonials</h4>
            <img src="images/t.png" alt="" />
            <h5>Ive been a member of maximum gym for over three years and recently encouraged several of my friends to join this clean, well-run facility. My workout is part of my cardiac rehab program and i have managed to loose over 13kg and lowered my heartrate and blood pressure significantly</h5>
            <p>Kwame Asante</p>
            <a href="" class="btn btn-1 btn-1d">Read Testimonials</a>
        </div>
    </div>
</div>
<!--/Support-->

<!-- packages -->
<div class="find-dog packages">
    <div class="container relative">
        <h4>ACTIVITIES</h4>

        @foreach($activities as $item)
            <div class="col-md-4 packageItem team-grid-left">
                <div class="team-top">
                    <img class="img-responsive" src="images/c1.jpg" alt="">
                </div>
                <h3>{{$item->title}}</h3>

                <p class="description">

                    {{$item->description}}

                </p>

                <p class="price"> {{$item->price}} GHC</p>
                <br>
                <a href="{{url('/home')}}" class="btn btn-primary">Buy</a>
            </div>
        @endforeach

    </div>
</div>
<!-- end packages-->


<!--gallery-->
<div id="gallery" class="gallery">
    <div class="container relative">
        <h4>IMAGES</h4>
        <div class="label-3"> </div>
        <div class="label-4"> </div>
        <h3><span class="left-icon"> </span> <span class="right-icon"> </span><div class="clearfix"> </div></h3>
        <!--Portfolio-->
        <div class="project spl-gallery">
            <div id="portfoliolist">
                <div class="portfolio" style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c5.jpg" class="b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="images/c5.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c2.jpg" class="b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="images/c2.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c3.jpg" class="b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="images/c3.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c4.jpg" class="b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="images/c4.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c5.jpg" class="b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="images/c5.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c6.jpg" class="b-link-stripe b-animate-go  swipebox"  title="Image Title">
                            <img src="images/c6.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c7.jpg" class="b-link-stripe b-animate-go   swipebox"  title="Image Title">
                            <img src="images/c7.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="portfolio"  style="display: inline-block; opacity: 1;">
                    <div class="portfolio-wrapper">
                        <a href="images/c8.jpg" class="b-link-stripe b-animate-go  swipebox"  title="Image Title">
                            <img src="images/c8.jpg" /><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 ">Max Gym<img src="images/p-search.png" /></h2>
                            </div></a>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!--/Portfolio-->
    </div>
</div>
<!--/gallery-->
<!--contact-->
<div class="contact" id="contact">
    <div class="container relative">
        <h4>Information</h4>

        <div class="col-md-5 contact-address">
            <div class="contact-top-one">
                <h5>ADDRESS:</h5>
                <h6>Maximum Gym <span>Around choice, Dansoman</span>Accra, Ghana.</h6>
            </div>
            <div class="contact-top-one">
                <h5>PHONES:</h5>
                <p>Telephone: +233 233300223 <span> +233302022233 </span></p>
            </div>
            <div class="contact-top-one">
                <h5>E-MAIL:</h5>
                <p><a>manager@maximumgym.com</a></p>
            </div>
        </div>
        <div class="col-md-5 contact-top-right">
            <form>
                <input type="text" placeholder="Name" required="">
                <input type="text" placeholder="Email" required="">
                <input type="text" placeholder="Subject" required="">
                <textarea placeholder="Message" required=""> </textarea>
                <div class="sub-button">
                    <input type="submit" value="SEND">
                </div>
            </form>
        </div>
    </div>
</div>
<!--/contact-->


@endsection