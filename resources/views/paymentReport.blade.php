<html>
<head>

    <link href="{{url('css/bootstrap.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="col-md-12 main " >
    <div>
        <img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
        <h3 class="logoHeader">Customer report </h3>
    </div>

    <style>
        td,th{
            text-align: center;
        }

    </style>

    <h3 style="color:#B79043">PAYMENT TRANSACTIONS REPORT</h3>
    <table class="table table-hover">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Txn ID</th>
        </tr>
        @foreach($transactions as $item)
            <tr>
                <td>
                    {{$item->User->fname}} {{$item->User->sname}}
                </td>
                <td>{{$item->User->email}}</td>
                <td>{{$item->User->phone}}</td>
                <td>{{$item->transaction}}</td>
            </tr>
        @endforeach
    </table>

</div>
</body>
</html>