<div class="container-fluid" style="background-color: #3D3D3D; padding: 0 50px;">
    <div class="banner-right-text">
        <h3>
            @if (Auth::check())
                <a class="btn btn-primary" href="{{ url('/home') }}">Dashboard</a>
                <a class="btn btn-danger" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

            @else
                <a class="btn btn-primary" href="{{ url('/login') }}">Login</a>
                <a class="btn btn-primary" href="{{ url('/register') }}">Register</a>
            @endif
            <br /><span>(+233)</span><i>233423401</i></h3>
    </div>
    <div class="top-menu">
        <span class="menu"> </span>
        <ul class="nav navbar-nav">
            <li><a class="active" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a></li>
            <li><a href='{{url('/#about')}}'>About</a></li>
            <li><a href="#">Trainers</a></li>
            <li><a href="{{url('/#testimonial')}}">Testimonials</a></li>
            <li><a href="{{url('/#gallery')}}">Gallery</a></li>
            <li><a href="{{url('/#contact')}}">Contact Us</a></li>
            <div class="clearfix"> </div>
        </ul>
    </div>
    <!-- script-for-menu -->
    <script>
        $("span.menu").click(function(){
            $(".top-menu ul").slideToggle("slow" , function(){
            });
        });
    </script>
    <!-- script-for-menu -->
</div>