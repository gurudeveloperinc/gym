@extends('layouts.app')

@section('content')
    @include('topMenu')

    <style>
        p.details{
            font-size: 150%;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Dashboard for {{Auth::user()->fname}} {{Auth::user()->sname}}
                        <br>You are a {{Auth::user()->role}}

                        <a href="{{url('/home')}}" class="btn btn-primary" style="float:right; margin-top:-10px;">Back</a>
                    </div>

                    <div class="panel-body">

                        {{-- manager --}}
                        <div>
                            <div align="center">
                                <img src="{{$user->photo}}" style="width:150px;" >
                            </div>

                            <p class="details">
                                <b>Name:</b> <span> {{$user->fname}} {{$user->mname}} {{$user->sname}} </span> <br>
                                <b>Email:</b> <span> {{$user->email}} </span> <br>
                                <b>Phone:</b> <span> {{$user->phone}}</span> <br>
                                <b>DOB:</b> <span> {{$user->age}} </span> <br>
                                <b>Postal address:</b><br> <span>{{$user->postalAddress}}</span> <br>
                                <b>Residential address:</b><br> {{$user->residentialAddress}} <br>
                                <b>Office Phone:</b> {{$user->officePhone}} <br>
                                <b>Home Phone:</b> {{$user->homePhone}} <br>
                                <b>Complications:</b> {{$user->complications}} <br>
                                <b>Weight:</b> {{$user->weight}} <br>
                                <b>Height:</b> {{$user->height}}<br>
                                <b>Reason:</b> {{$user->reason}}<br>
                                @if(count($user->Purchases) - 1 >= 0)
                                <b>Package:</b> {{$user->Purchases[count($user->Purchases) - 1 ]->Package->title}}
                                @endif

                            </p>


                            <div>

                                @if($user->role == "Instructor" && !empty($assigned))
                                    <h3 align="center">Assigned To</h3>
                                    <ul style="text-align: center">
                                        @foreach($assigned as $item)
                                        <li style="font-size: 20px">{{$item->fname}} {{$item->sname}}</li>
                                        @endforeach

                                    </ul>

                                @endif


                            @if($user->role == "Customer" && empty($assigned))

                                <h3>Assign Instructor</h3>
                                    <form method="post" action="{{url('/assign-instructor')}}">
                                        {{csrf_field()}}
                                        <select name="instructor" class="form-control">
                                            @foreach($instructors as $item)
                                                <option value="{{$item->uid}}">{{$item->fname}} {{$item->sname}}</option>
                                            @endforeach
                                        </select>

                                        <input type="hidden" value="{{$user->uid}}" name="client">

                                        <button type="submit" class="btn btn-primary">Assign</button>
                                    </form>

                                @endif

                                @if($user->role == "Customer" && !empty($assigned) )

                                    <p style="font-size: 24px;" align="center">
                                        Assigned to {{$assigned->fname}} {{$assigned->sname}}
                                    </p>

                                @endif
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection